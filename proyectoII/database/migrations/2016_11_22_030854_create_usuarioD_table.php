<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioDTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarioD', function (Blueprint $table) {
          $table->increments('id');
          $table->string('cedula');
          $table->string('nombre');
          $table->string('apellido');
          $table->string('edad');
          $table->string('direccion');
          $table->string('nacionalidad');
          $table->string('telefono');
          $table->string('correo_electronico');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarioD');

    }
}
