<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;

class socialAccountService
{


	public function createOrGetUser(ProviderUser $providerUser){

		    $account = socialAccountModel::whereProvider('facebook')
		    ->whereProviderUserId($providerUser->getId())
		    ->first();

		    if($account){
		    	return $account->user;
		    }else{
		    	$account = new socialAccountModel([

		    		'provider_user_id' => $providerUser->getId(),
		    		'provider' => 'facebook'

				]);

		   //  echo $providerUser->getName();
		   //  echo $providerUser->getEmail();
			// echo $providerUser->getId();
			// echo $providerUser->token;


		    $user = User::whereEmail($providerUser->getEmail())->first();

		  
		    if(!$user){

		    	$user = User::create([
		    		'email' => $providerUser->getEmail(),
		    		'name' => $providerUser->getName(),
		    		'password' => $providerUser->token
		    		]);
		    }
		    $account->user()->associate($user);
		    $account->save();
		    return $user;



		    }

	}
}
