<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class socialAccountModel extends Model
{
    protected $table = 'social_accounts';
    protected $fillable = [
		'user_id','provider_user_id', 'provider'

    ];

   
    public function User(){
    	return $this->belongsTo(User::class);
    }
}
