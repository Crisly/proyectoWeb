<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\bienesModel;

class bienesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $bienes = bienesModel::all();
        return view('Bienes.index',['bienes'=> $bienes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('Bienes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'nombre'=>'required',
          'descripcion'=>'required',
          'correo_electronico'=>'required',
          'telefono'=>'required',
          'lugar'=>'required',
      ]);

     

      $bien = new bienesModel;
      $bien->nombre = $request->nombre;
      $bien->descripcion = $request->descripcion;
      $bien->correo_electronico = $request->correo_electronico;
      $bien->telefono = $request->telefono;
      $bien->lugar = $request->lugar;
      $bien->save();

      return redirect('Bienes')->with('message','data has been updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $bien = bienesModel::find($id);

        return view('Bienes.detail')->with('bien',$bien);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bien = bienesModel::find($id);
       
        return view('Bienes.edit')->with('bien',$bien);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
          'nombre'=>'required',
          'descripcion'=>'required',
          'correo_electronico'=>'required',
          'telefono'=>'required',
          'lugar'=>'required',
          ]);

     

      $bien = bienesModel::find($id);
      $bien->nombre = $request->nombre;
      $bien->descripcion = $request->descripcion;
      $bien->correo_electronico = $request->correo_electronico;
      $bien->telefono = $request->telefono;
      $bien->lugar = $request->lugar;
      $bien->save();

      return redirect('Bienes')->with('message','data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bien = bienesModel::find($id);
        $bien->delete();
        return redirect('Bienes')->with('message','data has been deleted!');
    }
}
