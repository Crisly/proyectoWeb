<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\usuariosModel;

class usuariosController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = usuariosModel::all();
        return view('Usuario.index',['usuarios'=> $usuarios]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'cedula'=>'required',
          'nombre'=>'required',
          'apellido'=>'required',
          'edad'=>'required',
          'direccion'=>'required',
          'nacionalidad'=>'required',
          'telefono'=>'required',
          'correo_electronico'=>'required',
      ]);

        

      $usuarios = new usuariosModel;
      $usuarios->cedula = $request->cedula;
      $usuarios->nombre = $request->nombre;
      $usuarios->apellido = $request->apellido;
      $usuarios->edad = $request->edad;
      $usuarios->direccion = $request->direccion;
      $usuarios->nacionalidad = $request->nacionalidad;
      $usuarios->telefono = $request->telefono;
      $usuarios->correo_electronico = $request->correo_electronico;
      $usuarios->save();

      return redirect('../home')->with('message','data has been updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = usuariosModel::find($id);
        return view('Usuario.detail')->with('usuario',$usuario);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = usuariosModel::find($id);
       
        return view('Usuario.edit')->with('usuario',$usuario);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'cedula'=>'required',
          'nombre'=>'required',
          'apellido'=>'required',
          'edad'=>'required',
          'direccion'=>'required',
          'nacionalidad'=>'required',
          'telefono'=>'required',
          'correo_electronico'=>'required',
      ]);

         $table->increments('id');
          $table->string('cedula');
          $table->string('nombre');
          $table->string('apellido');
          $table->string('edad');
          $table->string('direccion');
          $table->string('nacionalidad');
          $table->string('telefono');
          $table->string('correo_electronico');

      $usuarios = usuariosModel::find($id);
      $usuarios->cedula = $request->cedula;
      $usuarios->nombre = $request->nombre;
      $usuarios->apellido = $request->apellido;
      $usuarios->edad = $request->edad;
      $usuarios->direccion = $request->direccion;
      $usuarios->nacionalidad = $request->nacionalidad;
      $usuarios->telefono = $request->telefono;
      $usuarios->correo_electronico = $request->correo_electronico;
      $usuarios->save();

      return redirect('Usuario')->with('message','data has been edit!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = usuariosModel::find($id);
        $usuario->delete();
        return redirect('Usuario')->with('message','data has been deleted!');
    }
}
