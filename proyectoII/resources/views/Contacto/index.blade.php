@extends('layouts.app')

@section('content')


  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Mensajes enviados</div>
                <table class="table">
                  <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Correo Electronico</th>
                            <th>Mensaje</th>
                  
                          </tr>

                          <br>
                  </thead>

                  <tbody>

                    @foreach ($contactos as $contacto)
                                  <tr>
                                      <th scope="row">{{ $contacto->nombre }}</th>
                                      <td>{{ $contacto->correo_electronico }}</td>
                                      <td>{{ $contacto->mensaje }}</td>
                                    </tr>
                                    <br>
                                    
                                    <tr>
                                  </tr>
                    @endforeach
            </tbody>
            </table>

              <br>
              <br>
             <a href="../home" class="btn btn-warning" role="button"> Volver </a>
          </div>
        </div>
      </div>

   </div>
   @endsection 