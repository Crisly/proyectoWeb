@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-heading">Enviar mensaje</div>

                 <div class="panel-body">
                    <form class="form-horizontal" role="form" action="/Contacto" method="post">

                        <div class="form-group">
                            <label for="nombre" class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="nombre" value="" placeholder="">
                                {{ ($errors->has('cedula')) ? $errors->first('cedula') : '' }}
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="correo_electronico" class="col-md-4 control-label">Correo electronico</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="correo_electronico" value="" placeholder="gonzalezcrisly@gmail.com">
                                {{ ($errors->has('correo')) ? $errors->first('correo') : '' }}
                            </div>
                        </div>



                         <div class="form-group">
                            <label for="mensaje" class="col-md-4 control-label">Mensaje</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="mensaje" rows="2" cols="40" placeholder=""></textarea>
                                 {{ ($errors->has('mensaje')) ? $errors->first('mensaje') : '' }}
                            </div>
                        </div>

                        

                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input class="btn btn-primary btn-block" type="submit" name="name" value="Crear">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection