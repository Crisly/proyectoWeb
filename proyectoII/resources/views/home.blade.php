@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                        <img alt="Brand" src="http://www.bncr.fi.cr/BNCR/images/logo.gif">
                        </a>
                    </div>                   
                </div>
                </nav>


                 <div>
                        <nav class="navbar navbar-inverse">
                    <section class="container-fluid">
                        <section class="navbar-header">
                            <a class="navbar-brand" href="#">Venta Bienes & Raices</a>
                        </section>
                         
                        <ul class="nav navbar-nav">
                          
                            <li class="dropdown">
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> Bienes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="../Bienes/create">Agregar Bienes</a></li>
                                  <li><a href="../Bienes/">Gestionar Bienes</a></li> 
                                 
                                   
                                </ul>
                             </li>

                             <li class="dropdown">
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> Contacto <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="../Contacto/create">Contacto</a></li> 
                                  <li><a href="../Contacto/">Mensajes enviados</a></li> 
                                </ul>
                             </li>



                             <li class="dropdown">
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> Usuarios <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="../Usuario/create">Agregar Usuarios</a></li> 
                                  <li><a href="../Usuario/">Gestionar Usuarios</a></li> 
                                </ul>
                             </li>





                        </ul>
                    </section>
                   </nav>

                    </div>



                <div class="panel-body">
                    
                    
                </div>



                <div class="col-md-8 >
                    
                    <section class="container" id="footer">
                    <section class="panel panel-default">
                        <section class="panel-footer">Fin de la pagina</section>
                    </section>
                    </section>

                </div>


                

            </div>
            
        </div>
    </div>
</div>
@endsection
