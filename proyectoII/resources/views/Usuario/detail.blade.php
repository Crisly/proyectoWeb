@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Detalle</div>

                 <div class="panel-body">

					 <p>Nombre: {{ $cliente->nombre }}</p>

					  <p>Apellido: {{ $cliente->apellido }}</p>


					  <p>Cedula: {{ $cliente->cedula }}</p>

					  <p>Fecha Nacimiento: {{ $cliente->fecha_nacimiento }}</p>


					  <p>Dirección: {{ $cliente->direccion }}</p>

					  <p>Estado Civil: {{ $cliente->estado_civil }}</p>


					  <p>Sexo: {{ $cliente->sexo }}</p>

					  <p>Fecha Ingreso: {{ $cliente->fecha_ingreso }}</p>

					  <p>Tipo: {{ $cliente->tipo }}</p>

					  <p>Descuento: {{ $cliente->descuento }}</p>

					   <a href="../home" class="btn btn-warning" role="button"> Volver </a>
		    	</div>
		    </div>
		</div>
	</div>
</div>
@endsection 


