@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"> Editando Usuario </div>

                 <div class="panel-body">

                      <form class="" action="/Usuario/{{ $usuario->id}}" method="post">
                      
                        <div class="form-group">
                            <label for="cedula" class="col-md-4 control-label">Cedula</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="cedula" value="{{usuario->cedula}}" placeholder="">
                                {{ ($errors->has('cedula')) ? $errors->first('cedula') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombre" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="nombre" value="{{usuario->nombre}}" placeholder="">
                                {{ ($errors->has('nombre')) ? $errors->first('nombre') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="apellido" class="col-md-4 control-label">Apellido</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="apellido" value="{{usuario->apellido}}" placeholder="">
                                {{ ($errors->has('apellido')) ? $errors->first('apellido') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edad" class="col-md-4 control-label">Edad</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="edad" value="{{usuario->edad}}" placeholder="">
                                {{ ($errors->has('edad')) ? $errors->first('edad') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="direccion" class="col-md-4 control-label">Direccion</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="direccion" rows="2" cols="40" placeholder="">{{usuario->direccion}}</textarea>
                                 {{ ($errors->has('direccion')) ? $errors->first('direccion') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nacionalidad" class="col-md-4 control-label">Nacionalidad</label>
                            <div class="col-md-6">
                                 <textarea class="form-control" name="nacionalidad" rows="2" cols="40" placeholder="">{{usuario->nacionalidad}}</textarea>
                                 {{ ($errors->has('nacionalidad')) ? $errors->first('nacionalidad') : '' }}
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <label for="telefono" class="col-md-4 control-label">Telefono</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="telefono" rows="2" cols="40" placeholder="">{{usuario->telefono}}</textarea>
                                 {{ ($errors->has('nacionalidad')) ? $errors->first('nacionalidad') : '' }}
                            </div>
                            
                        </div>



                        <div class="form-group">
                            <label for="correo_electronico" class="col-md-4 control-label">Correo electronico</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="correo_electronico" rows="2" cols="40" placeholder="">{{usuario->correo_electronico}}</textarea>
                                 {{ ($errors->has('correo_electronico')) ? $errors->first('correo_electronico') : '' }}
                            </div>
                            
                        </div>
                        </div>


                       
                      <input type="hidden" name="_method" value="put">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="submit" name="name" value="edit">
                  </form>
                </div>

            </div>

        </div>

    </div>
  
  </div>

 @endsection 




