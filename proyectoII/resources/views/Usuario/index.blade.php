@extends('layouts.app')

@section('content')


  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Usuarios Registrados</div>

                  <table class="table">
                      <thead>
                              <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Edad</th>
                                <th>Cedula</th>
                                <th>Dirección</th>
                                <th>Nacionalidad</th>
                                <th>Telefono</th>
                                <th>Correo electronico</th>
                      
                              </tr>

                              <br>
                      </thead>

                  <tbody>

                    @foreach ($usuarios as $usuario)
                                  <tr>
                                      <th scope="row">{{ $usuario->nombre }}</th>
                                      <td>{{ $usuario->apellido }}</td>
                                      <td>{{ $usuario->edad }}</td>
                                      <td>{{ $usuario->cedula }}</td>
                                      <td>{{ $usuario->direccion }}</td>
                                      <td>{{ $usuario->nacionalidad }}</td>
                                      <td>{{ $usuario->telefono }}</td>
                                      <td>{{ $usuario->correo_electronico }}</td>
                                        
                                    </tr>
                                    <br>
                                    
                                    <tr>
                                        <td>
                                            <a href="/Usuarios/{{ $usuario->id }}" class="btn btn-info" role="button"> Detalle </a>

                                            <a href="/Usuarios/{{ $usuario->id }}/edit" class= "btn btn-success" role="button">Editar</a>

                                            <form class="" action="/Usuarios/{{ $usuario->id }}" method="post">
                                                          <input type="hidden" name="_method" value="delete">
                                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                          <input class= "btn btn-danger"  type="submit" name="name" value="delete">
                                            </form>
                                        </td>
                                  </tr>
                    @endforeach
            </tbody>
            </table>

              <br>
              <br>
              <a href="../home" class="btn btn-warning" role="button"> Volver </a>

              </div>
        </div>
    </div>
  </div>
   @endsection 