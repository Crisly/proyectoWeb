@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-heading">Agregar Bienes </div>

                 <div class="panel-body">
                    <form class="form-horizontal" role="form" action="/Bienes" method="post">

                        <div class="form-group">
                            <label for="nombre" class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="nombre" value="" placeholder="Finca San Luis">
                                {{ ($errors->has('cedula')) ? $errors->first('cedula') : '' }}
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="descripcion" class="col-md-4 control-label">Descripción</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="descripcion" rows="2" cols="40" placeholder=""></textarea>
                                 {{ ($errors->has('descripcion')) ? $errors->first('descripcion') : '' }}
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="correo_electronico" class="col-md-4 control-label">Correo electronico</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="correo_electronico" value="" placeholder="gonzalezcrisly@gmail.com">
                                {{ ($errors->has('correo')) ? $errors->first('correo') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="telefono" class="col-md-4 control-label">Telefono</label>

                            <div class="col-md-6">
                                <input class="form-control" type="text" name="telefono" value="" >
                                {{ ($errors->has('telefono')) ? $errors->first('telefono') : '' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lugar" class="col-md-4 control-label">Lugar</label>

                            <div class="col-md-6">
                                 <textarea class="form-control" name="lugar" rows="2" cols="40" placeholder=""></textarea>
                                 {{ ($errors->has('lugar')) ? $errors->first('lugar') : '' }}
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input class="btn btn-primary btn-block" type="submit" name="name" value="Crear">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection