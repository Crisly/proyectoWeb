@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Detalled de un bien</div>

                 <div class="panel-body">

					 <p>Nombre: {{ $bien->nombre }}</p>

					  <p>Descripción: {{ $bien->descripcion }}</p>


					  <p>Correo electronico: {{ $bien->correo_electronico }}</p>

					  <p>Telefono: {{ $bien->telefono }}</p>


					  <p>Lugar: {{ $bien->lugar }}</p>

					   <a href="../home" class="btn btn-info" role="button"> Volver </a>
		    	</div>
		    </div>
		</div>
	</div>
</div>
@endsection 