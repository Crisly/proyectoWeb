@extends('layouts.app')

@section('content')


  <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Bienes en Registro Registrados</div>
                <table class="table">
                  <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Correo Electronico</th>
                            <th>Telefono</th>
                             <th>Lugar</th>
                  
                          </tr>

                          <br>
                  </thead>

                  <tbody>

                    @foreach ($bienes as $bien)
                                  <tr>
                                      <th scope="row">{{ $bien->nombre }}</th>
                                      <td>{{ $bien->descripcion }}</td>
                                      <td>{{ $bien->correo_electronico }}</td>
                                      <td>{{ $bien->telefono }}</td>
                                       <td>{{ $bien->lugar }}</td>
                                        
                                    </tr>
                                    <br>
                                    
                                    <tr>
                                        <td>
                                            <a href="/Bienes/{{ $bien->id }}" class="btn btn-info" role="button"> Detalle </a>

                                            <a href="/Bienes/{{ $bien->id }}/edit" class= "btn btn-success" role="button">Editar</a>

                                            <form class="" action="/Bienes/{{ $bien->id }}" method="post">
                                                          <input type="hidden" name="_method" value="delete">
                                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                          <input class= "btn btn-danger"  type="submit" name="name" value="delete">
                                            </form>
                                        </td>
                                  </tr>
                    @endforeach
            </tbody>
            </table>
          </div>
        </div>
      </div>

   </div>
   @endsection 